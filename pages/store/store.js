import { createStore } from "redux";
const CHANGE_BITCOIN_DATA = "CHANGE_BITCOIN_DATA";

const defaultValue = {
  bitcoinData: null,
};

function bitcoinReducer(state = defaultValue, action) {
  switch (action.type) {
    case CHANGE_BITCOIN_DATA:
      return { ...state,bitcoinData: action.payload };
    default:
      return state;
  }
}

export const store = createStore(bitcoinReducer);

export const changeBitcoinDataAction = (payload) => {
  return { type: CHANGE_BITCOIN_DATA, payload };
};