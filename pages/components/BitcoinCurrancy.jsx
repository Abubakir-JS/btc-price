import Image from "next/image";
import React from "react";
import USD from "../../image/usd.jpg";
import GBP from "../../image/sterling.jpg";
import EUR from "../../image/euro.webp";
import { useSelector } from "react-redux";
import styles from "../../styles/BitcoinCurrency.module.scss";

const BitcoinCurrency = () => {
  const bpi =
    useSelector(
      (state) =>
        state?.bitcoinData?.bpi && Object.values(state?.bitcoinData?.bpi)
    ) || [];
  const images = {
    USD: USD,
    GBP: GBP,
    EUR: EUR,
  };

  const localeCurency = (code) => {
    if(code === 'USD') {
        return '$'
    }
    if(code === 'EUR') {
        return '€'
    }
    if(code === 'GBP') {
        return '£'
    }
  };


  return (
    <div className={styles.bitcoinCurrencyContainer}>
      {bpi.length !== 0 &&
        bpi.map(({ code, rate = "", rate_float = "", description }, index) => {
          console.log(code, rate, rate_float, description, "i am");
          return (
            <div className={styles.bitcoinContainer} key={index}>
              <p className={styles.currency}>{code}</p>
              <Image
                className={styles.images}
                src={images[code]}
                width={200}
                height={200}
                alt=""
              />
              <p className={styles.rate}>{localeCurency(code)}{rate}</p>
              <p className={styles.rateFloat}>{localeCurency(code)}{rate_float}</p>
              <p className={styles.description}>{description}</p>
            </div>
          );
        })}
    </div>
  );
};

export default BitcoinCurrency;
