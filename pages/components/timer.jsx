import styles from "../../styles/timer.module.scss";
import nextButon from "../../image/nextButton.svg";
import Image from "next/image";
import { useSelector } from "react-redux";
import { useState } from "react";

const Timers = () => {
  const [key, setKey] = useState("UTC");
  const bitcoindataTime =
    useSelector(
      (state) =>
        state?.bitcoinData?.time && Object.values(state?.bitcoinData?.time)
    ) || [];
  const times = {
    UTC: bitcoindataTime[0],
    BST: bitcoindataTime[2],
  };
  const convertDateToTime = (date = "") => {
    const index = date.split("").findIndex((item) => item === ":") - 2;
    const time = date
      .split("")
      .slice(index, index + 5)
      .join("");
    return time;
  };

  const moveHandler = () => {
    if (key === "UTC") {
      setKey("BST");
    } else if (key === "BST") {
      setKey("UTC");
    }
  };

  const time = times[key];

  return (
    <div className={styles.timerContainer}>
      <Image
        src={nextButon}
        alt=""
        width={80}
        height={80}
        className={styles.leftButton}
        onClick={moveHandler}
      />
      <div className={styles.timerWrapper}>
        <p className={styles.timer}>{convertDateToTime(time)}</p>
        <p className={styles.code}>{key}</p>
      </div>
      <Image
        src={nextButon}
        alt=""
        width={80}
        height={80}
        className={styles.rightButton}
        onClick={moveHandler}
      />
    </div>
  );
};

export default Timers;
